# SMBIOS-list repo  


This repo contains all the latest SMBIOS data from Clover EFI in xml format.  

The purpose of this repo is to provide an easy way to keep the SMBIOS data up to date in any app

[Official Thread](http://www.insanelymac.com/forum/topic/313285-smbios-list-xml/)  


## Usage

1. Run autogen.sh to create the smbios-list.xml file containing all the SMBIOS xmls.  
2. Use this file in your app.

## How to help


**To add a new xml:**  

* use an already existent xml as base and make your changes
* Open a new PR or reply [here](http://www.insanelymac.com/forum/topic/313285-smbios-list-xml/)  

## Apps that are using SMBIOS-list
* [Cloud Clover Editor](http://cloudclovereditor.altervista.org/cce/index.php)


## Credits

wardoctor - iMac17,1 / iMac16,2  
Clover EFI devs  
mackie100  
pikeralpha  
Sherlocks - Clover EFI platform data updates  
PMheart - Clover EFI platform data updates